#!/usr/bin/env python
# coding: utf-8

# # World Covid-19 Vaccination Analysis using operations of ML
# #By- Aarush Kumar
# #Dated: June 28,2021

# In[2]:


import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
import plotly.express as px
import plotly.graph_objs as go
from plotly.offline import iplot, init_notebook_mode
import cufflinks
cufflinks.go_offline(connected=True)
init_notebook_mode(connected=True)
import warnings
warnings.filterwarnings("ignore")


# In[3]:


manu =pd.read_csv("/home/aarush100616/Downloads/Projects/World Covid Vaccination Analysis/country_vaccinations_by_manufacturer.csv")
manu['date'] = pd.to_datetime(manu.date)

country =pd.read_csv("/home/aarush100616/Downloads/Projects/World Covid Vaccination Analysis/country_vaccinations.csv")
country['date'] = pd.to_datetime(country.date)


# In[4]:


manu.head()


# In[5]:


manu.location.unique()


# In[6]:


#temporarily set max max_colwidth to None
pd.set_option('display.max_colwidth', None)
country[['country', 'date', 'vaccines']].head()


# In[7]:


#strip text
country['vaccines'] = country['vaccines'].str.replace("/Beijing|/Wuhan|/HayatVax|/BioNTech|Oxford/", "", regex=True)
#replace with Chinese vaccins
country['vaccines'] = country['vaccines'].str.replace("Sinopharm|BBIBP-CorV|Sinovac|CanSino|RBD-Dimer", "Chinese", regex=True)
#replace with Russian vaccins
country['vaccines'] = country['vaccines'].str.replace("EpiVacCorona|Sputnik V", "Russian", regex=True)
#replace with Cuban vaccins
country['vaccines'] = country['vaccines'].str.replace("Soberana02|Abdala", "Cuban", regex=True)
#replace some others
to_replace = {'Covaxin': 'Indian',
              'QazVac': 'Kazachstan'}
country['vaccines'] = country['vaccines'].replace(to_replace, regex=True)


# In[8]:


#making a list of all vaccins
vac_list = [x.split(", ") for x in country.vaccines.values]
vaccins = [item for elem in vac_list for item in elem]
vaccins = set(vaccins)
vaccins = list(vaccins)
vaccins


# In[9]:


#adding a column with True/False for each vaccin
for vaccin in vaccins:
    country[vaccin] = np.where(country['vaccines'].str.contains(vaccin), True, False)
country = country.sort_values(by = ['country', 'date'], ascending = [True, False])
country_latest = country.drop_duplicates(subset = "country", keep = "first")
#head of selected columns only
country_latest.iloc[:, np.r_[0,12, 15:len(country_latest.columns)]].head()


# In[10]:


def plot_vaccin(color, vaccin):
    fig = px.choropleth(country_latest, locations="iso_code",
                        color=vaccin,
                        hover_name="country",
                        color_discrete_map={True: color, False: 'lightgrey'})
    layout = go.Layout(
        title=go.layout.Title(
            text= f"<b>Countries using {vaccin} vaccin</b>",
            x=0.5
        ),
        showlegend=False,
        font=dict(size=14),
        width = 750,
        height = 350,
        margin=dict(l=0,r=0,b=0,t=30)
    )
    fig.update_layout(layout)
    fig.show()


# In[11]:


plot_vaccin('red', 'Pfizer')


# In[12]:


plot_vaccin('blue', "Moderna")


# In[13]:


plot_vaccin('green', "AstraZeneca")


# In[14]:


plot_vaccin('magenta', "Johnson&Johnson")


# In[15]:


plot_vaccin('brown', "Chinese")


# In[16]:


plot_vaccin('grey', "Russian")


# In[17]:


plot_vaccin('black', "Indian")


# In[21]:


plot_vaccin('indigo', "Cuban")


# In[22]:


plot_vaccin('darkblue', "Kazachstan")


# ## Worldwide map plotting the number of vaccines used by country

# In[23]:


country_latest['Vaccins_used']= country_latest.iloc[:, -9:].sum(axis=1)
country_latest.Vaccins_used.value_counts()


# In[24]:


#function as I will make a similar map later
def plot_ww_numbers(data, color, hover_data, title):
    fig = px.choropleth(data, locations="iso_code",
                        color= color,
                        hover_data= hover_data)
    layout = go.Layout(title=go.layout.Title(
        text= f"<b>{title}</b>",
        x=0.5
        ),
        font=dict(size=14),
        width = 750,
        height = 350,
        margin=dict(l=0,r=0,b=0,t=30)
                      )
    fig.update_layout(layout)
    fig.show()


# In[25]:


plot_ww_numbers(data = country_latest,
                color = 'Vaccins_used',
                hover_data= ["country", "vaccines"],
                title = 'Number of different vaccines used by country')


# ### Usage of vaccines in Europe by country

# In[26]:


non_european = ['Chile', 'United States', 'Uruguay']
manu = manu.query('location not in @non_european')
manu.location.nunique()


# In[27]:


manu[manu.location == "Netherlands"].tail(8)


# In[28]:


manu_totals = manu.copy()
#keeping only the latest info
manu_totals = manu_totals.sort_values(by = ['location', 'date', 'vaccine'], ascending = [True, False, True])
manu_totals = manu_totals.drop(columns = "date")
manu_totals = manu_totals.drop_duplicates(subset = ['location', 'vaccine'], keep = "first")
manu_totals = manu_totals.pivot(index = ['location'], columns = 'vaccine', values = 'total_vaccinations')
manu_totals = manu_totals.fillna(0)
manu_totals.iloc[:, -6:] = manu_totals.iloc[:, -6:].astype(int)
#convert to percent
cols = list(manu_totals.columns)
manu_totals[cols] = manu_totals[cols].div(manu_totals[cols].sum(axis=1), axis=0).multiply(100)
manu_totals = manu_totals.round(1)                    
#adding ISO codes from my own dataset https://www.kaggle.com/erikbruin/countries-of-the-world-iso-codes-and-population
countries_iso = pd.read_csv("/home/aarush100616/Downloads/Projects/World Covid Vaccination Analysis/country_codes_2020.csv")
countries_iso = countries_iso.rename(columns = {'name': 'location', 'cca3': 'iso_code'})
manu_totals = manu_totals.reset_index()
manu_totals = manu_totals.merge(countries_iso[['location', 'iso_code']], on = 'location', how = "left")


# In[29]:


manu_totals['mRNA'] = manu_totals['Moderna'] + manu_totals['Pfizer/BioNTech']
manu_totals.head()


# In[30]:


fig = px.choropleth(manu_totals, locations="iso_code",
                    color='mRNA',
                    hover_data= ["location"])
layout = go.Layout(
    title=go.layout.Title(
        text= f"<b>Percent mRNA vaccines used by country</b>",
        x=0.5
    ),
    font=dict(size=14),
    autosize=False,
    width = 800,
    height = 600,
    margin=dict(l=0,r=0,b=0,t=30)
)
fig.update_geos(scope="europe")
fig.update_layout(layout)
fig.show()


# In[31]:


manu_totals[manu_totals.location == "Hungary"]


# ### Usage of vaccines in Europe over time

# In[32]:


manu = manu.sort_values(by=['location', 'vaccine', 'date'], ascending = [True, True, False])
manu = manu.reset_index(drop=True)
manu['new'] = 0
for i in range(len(manu)-1):
    if ((manu.loc[i+1, 'vaccine'] == manu.loc[i, 'vaccine']) & (manu.loc[i+1, 'location'] == manu.loc[i, 'location'])):
        manu.loc[i, 'new'] = (manu.loc[i, 'total_vaccinations'] - manu.loc[i+1, 'total_vaccinations'])
    else:
        manu.loc[i, 'new'] = manu.loc[i, 'total_vaccinations']
manu.head()


# In[33]:


new_vaccines = manu[['date', 'vaccine', 'new']]
df = new_vaccines[['date', 'vaccine']]
df.drop_duplicates(inplace = True)
df = df.sort_values(by = ['vaccine', 'date'], ascending = [True, False]).reset_index(drop=True)
df['before'] = 0
for i,row in df.iterrows():
    total_before = new_vaccines[((new_vaccines.vaccine == row.vaccine) & (new_vaccines.date <= row.date))]['new'].sum()
    df.loc[i, 'before'] = total_before  
df = df.pivot(index= 'date', columns = 'vaccine', values = 'before')
df = df[['Johnson&Johnson', 'Moderna', 'Oxford/AstraZeneca', 'Pfizer/BioNTech']] #getting rid of Sputnik and Sinopharm
df.head()


# In[34]:


df.iplot(mode = 'lines', xTitle = 'Date', yTitle = 'Total vaccinations', title = 'Cumulative vaccinations in Europe by vaccine')


# ## Vaccination rates by country

# In[35]:


#calculating population
country_latest['population'] = round((country['total_vaccinations']/country['total_vaccinations_per_hundred']*100), 0)
cols_to_keep = ['country', 'iso_code', 'total_vaccinations', 'total_vaccinations_per_hundred', 'population']
country_latest[cols_to_keep].sort_values(by = "total_vaccinations_per_hundred", ascending = [False]).head(10)


# In[36]:


#rename to make name short for legend
country_latest = country_latest.rename(columns = {'total_vaccinations_per_hundred': 'per 100'})
#taking out very small countries at least gets rid of outlier Gibraltar (want to keep Emirates with nearly 10 million population)
plot_ww_numbers(data = country_latest[country_latest.population > 50000],
                color = 'per 100',
                hover_data= ["country"],
                title = 'Total vaccines given per country per 100 inhabitants')


# ## Vaccination rates by continent

# In[37]:


cols_to_load = ['Three_Letter_Country_Code', 'Continent_Name']
continents = pd.read_csv("/home/aarush100616/Downloads/Projects/World Covid Vaccination Analysis/country-and-continent-codes-list-csv.csv")[cols_to_load]
continents = continents.rename(columns = {'Three_Letter_Country_Code': 'iso_code'})
country_latest = country_latest.merge(continents, on = "iso_code", how = "inner")
continent_latest = country_latest.copy()
continent_latest = continent_latest[['Continent_Name', 'country', 'iso_code', 'population', 'total_vaccinations']]
#assign Russia, Turkey to Asia
continent_latest = continent_latest.sort_values(by = ['iso_code', 'Continent_Name'], ascending = [True, True])
continent_latest = continent_latest.drop_duplicates(subset = "iso_code", keep = "first")
#info from 3 countries is missing. Remove them to keep it simple
continent_latest = continent_latest[continent_latest.total_vaccinations.notna()]
continent_latest[['population', 'total_vaccinations']] = continent_latest[['population', 'total_vaccinations']].astype(int)
continent_latest['continent_population'] = continent_latest.groupby('Continent_Name')['population'].transform('sum')
continent_latest['continent_vaccinations'] = continent_latest.groupby('Continent_Name')['total_vaccinations'].transform('sum')
continent_latest['per 100'] = round(((continent_latest['continent_vaccinations']/continent_latest['continent_population'])*100), 1)


# In[38]:


plot_ww_numbers(data = continent_latest,
                color = 'per 100',
                hover_data= ["country", "Continent_Name"],
                title = 'Total vaccines given per continent per 100 inhabitants')

